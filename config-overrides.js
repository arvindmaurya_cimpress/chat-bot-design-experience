// config-overrides.js
const path = require('path');

module.exports = function override(config, env) {
  // Add or modify the Webpack configuration here
  config.resolve.fallback = {
    //"path": require.resolve("path-browserify"),
    //"os": require.resolve("os-browserify/browser"),
    "fs":false,
    //"crypto": require.resolve("crypto-browserify"),
    //"stream": require.resolve("stream-browserify"),
    //"buffer": require.resolve("buffer/"),
    "stream": false,

    "buffer":false,
    "crypto":false,
    "os":false,
    "path":false



    
  };
  
  return config;
};
