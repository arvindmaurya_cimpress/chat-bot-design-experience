import natural from "natural";
import pluralize from "pluralize";
import colorName from "color-name";
import dotenv from "dotenv";
//dotenv.config();

var actionsList = ["reduce", "increase", "decrease"];
var generativeTerms = ["improve", "suggest", "better", "new"];
var subPropertyList = ["font size", "font color", "font colour", "font style"];

var propertTypeList = {
	text: ["name", "location", "tagline"],
	image: ["logo", "background"],
};

let imgs = [
	"https://uploads.documents.cimpress.io/v1/uploads/832f34a9-8a2c-4f31-a394-011ad90f8c7a~122?tenant=default",
"https://uploads.documents.cimpress.io/v1/uploads/032ae212-a106-42fb-a8bb-33abcaa3f230~122?tenant=default",
"https://uploads.documents.cimpress.io/v1/uploads/44a19b54-090d-4538-b1fe-e0dbcddf176c~122?tenant=default",
"https://uploads.documents.cimpress.io/v1/uploads/0ed23e3f-6ab4-460b-aab8-6f043738fdbb~122?tenant=default",
"https://uploads.documents.cimpress.io/v1/uploads/dc242a67-f3fa-4e62-94ef-5472f3f2d88b~122?tenant=default",
"https://uploads.documents.cimpress.io/v1/uploads/3389104e-6ca7-4121-92c0-1e8f598b25d1~122?tenant=default",
"https://uploads.documents.cimpress.io/v1/uploads/bb6dda75-c29f-4b94-b3f9-0e29b31e84fa~122?tenant=default",
"https://uploads.documents.cimpress.io/v1/uploads/e40a3632-20e3-4782-891d-8dc6997bea10~122?tenant=default",
"https://uploads.documents.cimpress.io/v1/uploads/547a00be-a016-46b4-9ff3-ff53600937f9~122?tenant=default",
"https://uploads.documents.cimpress.io/v1/uploads/240d920a-7564-41f5-849c-81088396d844~122?tenant=default",
"https://uploads.documents.cimpress.io/v1/uploads/c3c7e4ff-098b-431a-bc26-82efa039f527~122?tenant=default",
]
var templateMap = {
	tagline: [
		"b171ad73-f4f9-4950-aa2b-59ec464f8943_vpls_text_4",
		"",
		"d6c42f6e-f641-45b8-bf13-ebdbceb5c342_vpls_text_5",
	],
	location: ["f94f59f8-71c2-41ec-8b31-f128be4d2646_vpls_text_7", "", ""],

	logo: [
		"91046fb6-2b20-45b2-9200-f0c386204c22_vpls_img_3",
		"9497d6eb-1726-4b55-ba78-e78d67a6a2b0_vpls_img_1",
		"",
	],
	name: [
		"80300ce6-9a9b-454d-aef0-cf26d213d82c_vpls_text_5",
		"ae93a370-cf5f-4490-89dc-de159bd17a82_vpls_text_3",
		"34f5daca-b1b5-49a6-9bc7-54f212543721_vpls_text_4",
	],
	background: [
		"96f1da01-00ce-469c-8e86-2501869123e4_vpls_img_1",
		"03226462-7af5-4a5d-9a42-f891e890491f_vpls_img_2",
		"6cba45ab-f3b0-4b71-8edb-92d40735d619_vpls_img_2",
	],
};

function generateRandom(maxLimit = 100){
	let rand = Math.random() * maxLimit;
	console.log(rand); // say 99.81321410836433
  
	rand = Math.floor(rand); // 99
  
	return rand;
  }

const getTemplateMap =(cimDoc)=>{
	debugger;
	let tagline= cimDoc.metadata.template.find(x => x?.purpose == "businesstype")?.id;
	let location = cimDoc.metadata.template.find(x => x?.purpose == "address2")?.id;
	let name = cimDoc.metadata.template.find(x => x?.purpose == "companyname")?.id;
	let background = cimDoc.metadata.template[0]?.id;
	let logo = cimDoc.metadata.template[1]?.id;
	let templateMap={
		tagline:tagline,
		location:location,
		name:name,
		background:background,
		logo:logo

	};
	return templateMap;
}

function processPrompt(text) {
	const tokenizer = new natural.WordTokenizer();
	const words = tokenizer.tokenize(text);

	const filteredWords = words.filter(
		(word) => !natural.stopwords.includes(word.toLowerCase())
	);
	const stemmedWords = filteredWords.map((word) => pluralize.singular(word));
	return stemmedWords;
}

function getContentAfterInput(sentence, input) {
	const index = sentence.indexOf(input);
	if (index !== -1) {
		const inputEndIndex = index + input.length;
		return sentence.slice(inputEndIndex).trim();
	}
	return "";
}

function splitByStopWords(input) {
	const words = input.split(" ");
	const result = [];

	let phrase = "";
	for (const word of words) {
		if (natural.stopwords.includes(word.toLowerCase())) {
			if (phrase !== "") {
				result.push(phrase.trim());
				phrase = "";
			}
		} else {
			phrase += word + " ";
		}
	}

	if (phrase !== "") {
		result.push(phrase.trim());
	}

	return result;
}

function groupByTwo(list) {
	const result = [];

	for (let i = 0; i < list.length - 1; i++) {
		result.push(`${list[i]} ${list[i + 1]}`);
	}

	return result;
}

function classifyText(userPrompt) {
	var extractedWords = processPrompt(userPrompt);
	console.log(extractedWords);
	for (let i = 0; i < extractedWords.length; i++) {
		if (generativeTerms.includes(extractedWords[i].toLowerCase()))
			return "Generative";
	}

	return "Non-Generative";
}

function getKeyByWord(map, word) {
	for (const item of map) {
		const entry = Object.entries(item)[0];
		const key = entry[0];
		const list = entry[1];

		if (list.includes(word.toLowerCase())) {
			return key;
		}
	}

	return null;
}

function colorNametoRGB(color) {
	const lowercaseColor = color.toLowerCase();

	if (colorName.hasOwnProperty(lowercaseColor)) {
		const rgbValues = colorName[lowercaseColor];
		return `rgb(${rgbValues.join(",")})`;
	} else {
		return null;
	}
}

async function updateCimDoc(
	userPrompt,
	cimDoc,
	templateNumber,
	cimDocProperty,
	actionWord,
	updatedValue,
	subProperty = null
) {

	let templateMap = getTemplateMap(cimDoc);
	debugger;
	var promptType = classifyText(userPrompt);
	var propertyType = Object.keys(propertTypeList).find((key) =>
		propertTypeList[key].includes(cimDocProperty)
	);
	if (propertyType == "text" && promptType != "Generative") {
		var findId = templateMap[cimDocProperty];
		var textAreasList = cimDoc.document.panels[0].textAreas;
		for (let i = 0; i < textAreasList.length; i++) {
			if (textAreasList[i].id == findId) {
				if (subProperty != null) {
					if (subProperty == "font style") {
						textAreasList[i].content[0].fontStyle = updatedValue;
					} else if (subProperty == "font size") {
						if (updatedValue != undefined)
							textAreasList[i].content[0].fontSize = updatedValue + "pt";
						else if (actionWord == "increase") {
							var numericValue = parseFloat(
								textAreasList[i].content[0].fontSize
							);
							numericValue *= 1.2;
							textAreasList[i].content[0].fontSize = numericValue + "pt";
						} else if (actionWord == "decrease" || actionWord == "reduce") {
							var numericValue = parseFloat(
								textAreasList[i].content[0].fontSize
							);
							numericValue *= 0.8;
							textAreasList[i].content[0].fontSize = numericValue + "pt";
						}
					} else if (
						subProperty == "font color" ||
						subProperty == "font colour"
					) {
						var rgbColor = colorNametoRGB(updatedValue);
						textAreasList[i].content[0].color = rgbColor;
					}
				} else {
					textAreasList[i].content[0].content = updatedValue;
				}
			}
		}
		cimDoc.document.panels[0].textAreas = textAreasList;
	}
	debugger;
	if (propertyType == "image" && promptType == "Generative") {
		console.log("Generative prompt");
		//shubham code generated url should be replaces with the below
		var generatedImage =imgs[generateRandom(11)];
		var imagesList = cimDoc.document.panels[0].images;
		for (let i = 0; i < imagesList.length; i++) {
			var findId = templateMap[cimDocProperty];
			if (imagesList[i].id == findId) {
				imagesList[i].previewUrl = generatedImage;
				imagesList[i].printUrl = generatedImage;
			}
		}
		cimDoc.document.panels[0].images = imagesList;
	}
	// var token = process.env.JWT_TOKEN;
	// var udsOutput = await createDocref(cimDoc, token);
	// var previewUrl = `https://rendering.documents.cimpress.io/preview?width=1000&instructions_uri=${encodeURIComponent(
	// 	udsOutput
	// )}`;
	// var renderingUrl = `https://rendering.documents.cimpress.io/preview?width=1000&instructions_uri=${encodeURIComponent(
	// 	udsOutput
	// )}&scene=https%3A%2F%2Fcdn.scenes.documents.cimpress.io%2Fv3%2Fassets%2F9c67e9cf-34a2-4ce3-abe9-77d9fadba865%2Fcontent%0A`;

	return cimDoc;
	// return {
	// 	updatedCimDoc: cimDoc,
	// 	preview: previewUrl,
	// 	scenePreview: renderingUrl,
	// };
}

async function renderUserPrompt(cimDoc, templateId, userPrompt) {
	var cimDocProperty;
	var actionWord;
	var updatedValue;
	var subProperty;

	var words = processPrompt(userPrompt);

	var groupedWords = groupByTwo(words);

	var objectMap = [
		{ tagline: ["tagline"] },
		{ location: ["address", "location"] },
		{ logo: ["logo"] },
		{ name: ["name"] },
		{ background: ["background"] },
	];

	for (let i = 0; i < words.length; i++) {
		var word = words[i];
		var output = getKeyByWord(objectMap, word);
		if (output != null) cimDocProperty = output;
		if (actionsList.includes(word.toLowerCase()))
			actionWord = word.toLowerCase();
		if (!isNaN(Number(word))) updatedValue = word;
	}

	for (let i = 0; i < groupedWords.length; i++) {
		var groupedWord = groupedWords[i];
		if (subPropertyList.includes(groupedWord.toLowerCase()))
			subProperty = groupedWord.toLowerCase();
	}

	if (updatedValue == undefined) {
		var prop;
		if (subProperty != undefined) prop = subProperty;
		else prop = cimDocProperty;
		var splitSentence = getContentAfterInput(
			userPrompt.toLocaleLowerCase(),
			prop
		);
		var splitList = splitByStopWords(userPrompt);
		updatedValue =
			splitSentence != "" ? splitList[splitList.length - 1] : undefined;
	}

	console.log(cimDocProperty, subProperty, actionWord, updatedValue);

	if (cimDocProperty != undefined) {
		if (subProperty != undefined) {
			return await updateCimDoc(
				userPrompt,
				cimDoc,
				templateId,
				cimDocProperty,
				actionWord,
				updatedValue,
				subProperty
			);
		} else {
			return await updateCimDoc(
				userPrompt,
				cimDoc,
				templateId,
				cimDocProperty,
				actionWord,
				updatedValue
			);
		}
	}
	return null;
}

export {renderUserPrompt};
// FOR TESTING

// var doc = JSON.parse(fs.readFileSync("product_templates/1.json", "utf8"));

// var listOfPrompt = [
// 	"set company name to Cimpress India",
// 	"set location to Mumbai",
// 	"increase company name font size",
// 	"set tagline to Mass Customization Experts!",
// 	"update company name font style to bold",
// 	"change location font colour to black",
// 	"change company name font colour to black",
// 	"suggest better background",
// ];

// var finalOp;
// for (let i = 0; i < listOfPrompt.length; i++) {
// 	if (i == 0) finalOp = await renderUserPrompt(doc, 0, listOfPrompt[i]);
// 	else
// 		finalOp = await renderUserPrompt(finalOp.updatedCimDoc, 0, listOfPrompt[i]);
// }

// console.log(finalOp);
