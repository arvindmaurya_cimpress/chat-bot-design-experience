import React from 'react';
import {useContext} from 'react'
import { AppContext } from '../../AppContext';
import {processproductJson} from '../helperFunctions'
import {renderUserPrompt} from '../processPrompt'

const MessageParser = ({ children, actions }) => {

  const { imgUrl,dispatch } = useContext(AppContext);

  const parse = async (message) => {
debugger;
    if(message.includes('https://'))
    {
      actions.handleUrl(message);

    }
    if (message.includes('hello')) {
      actions.handleHello();
    }

    if (message.includes('dog')) {
      actions.handleDog();
    }

    if(true)
    {
      debugger;

      // let changeData = [];
      // changeData.push({'purpose':'companyname','key':'content','value': Math.random().toString()});
      // changeData.push({'purpose':'companyname','key':'color','value': Math.random().toString()});
      // changeData.push({'purpose':'companyname','key':'fontStyle','value': Math.random().toString()});
      // changeData.push({'purpose':'businesstype','value': Math.random().toString()});

      // let data = processproductJson(imgUrl,changeData);

      let data = await renderUserPrompt(imgUrl,2,message);
      dispatch({ type: "CLICK", imgUrl: data });
      actions.handleHello();
    }
  };

  return (
    <div>
      {React.Children.map(children, (child) => {
        return React.cloneElement(child, {
          parse: parse,
          actions
        });
      })}
    </div>
  );
};

export default MessageParser;