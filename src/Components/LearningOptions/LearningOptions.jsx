import React from "react";
import {useContext,useEffect,useState} from 'react'
import { AppContext } from '../../AppContext';

import {data} from '../../product_templates/Products'
import {generateRenderingUrl} from '../helperFunctions'


import "./LearningOptions.css";

const LearningOptions = (props) => {

  // Access dispatch function from context
  const { dispatch } = useContext(AppContext);


  const [productItems,setProductItems] = useState([]);

  useEffect(()=>{
    setProductItems(data);
  },[])

  const optionsMarkup = productItems.map((option) => (
    <button
      className="learning-option-button"
      key={option.id}
      onClick={() => dispatch({ type: 'CLICK', imgUrl:option})}
    >
      <img src={generateRenderingUrl(option)} />
    </button>
  ));

  return <div className="learning-options-container">{optionsMarkup}</div>;
};

export default LearningOptions;
