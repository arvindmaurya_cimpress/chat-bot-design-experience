
import Chatbot from "react-chatbot-kit";
import 'react-chatbot-kit/build/main.css'
import ActionProvider from './ChatBotHelper/ActionProvider';
import MessageParser from './ChatBotHelper/MessageParser';
import config from './ChatBotHelper/Config';



const DesignExperienceChatBot =(props) =>{
    
    return(
        <div>
            <Chatbot
                config={config}
                actionProvider={ActionProvider}
                messageParser={MessageParser}
            />
        </div>
    )
}
export default DesignExperienceChatBot;