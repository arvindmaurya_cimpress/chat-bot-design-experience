import React from "react";
import { AppContext } from '../AppContext';
import { useContext } from 'react'
import { createURLQuery } from './helperFunctions'
import { useEffect } from "react";
import {generateRenderingUrl} from './helperFunctions'



const DesignExperiencePreview = (props) => {

  // Access state and dispatch function from context
  const { imgUrl } = useContext(AppContext);
  return (
    <div className="design-experience">
      <div className="design-experience-container">
        <img src={generateRenderingUrl(imgUrl)} />
      </div>

    </div>
  )
}
export default DesignExperiencePreview;