const restAPICallAsync = async (apiURL, httpMethod, authorizationToken, params, httpBody, contentType) => {
    const endpointUrl = apiURL;
    var headers;
    if(contentType)
    {
        headers = {
            'Accept': 'application/json',
            'Authorization': authorizationToken === undefined ? undefined : 'Bearer ' + authorizationToken,
            'Content-Type': contentType
        }
    }
    else
    {
        headers = {
            'Accept': 'application/json',
            'Authorization': authorizationToken === undefined ? undefined : 'Bearer ' + authorizationToken
        }
    }
    const options = {
        method: httpMethod,
        headers: headers,
        body: httpBody,
    };

    if (!endpointUrl) {
        console.log("invalid endpoint url");
    }

    try{
        const response = await fetch((params === undefined ? endpointUrl : createURLQuery(params, endpointUrl)), options);
        const json = await response.json();
        return json;
    }
    catch(err)
    {
        return err;
    }

};

const mapToURLParameters = (obj) => {
    let params = Object.keys(obj).filter(key => obj[key] !== undefined && obj[key] !== null)
        .map((key, index) => `${key}=${encodeURIComponent(obj[key].toString())}`
        );
    return "?" + params.join("&");
};

const createURLQuery = (obj, endpointURL) => {
    let paramString = mapToURLParameters(obj);
    return endpointURL + paramString;
};

const generateInstructionUrl = (json) => {

    const instructionUrl = 'https://instructions.documents.cimpress.io/v3/instructions:preview';
    let params = {
      'documentUri': btoa(JSON.stringify(json)),
      'ignoreProjection': 'true'
    }
    return createURLQuery(params, instructionUrl);
  }

  const generateRenderingUrl = (json) => {
    const renderingurl = 'https://rendering.documents.cimpress.io/v1/cse/preview';
    const instructionUrl = generateInstructionUrl(json);
    let params = {
      'width': 1000,
      'height': 1000,
      'format': 'png',
      'instructions_uri': instructionUrl,
      'scene': 'https://scenes.documents.cimpress.io/v2/transient?width=1000&height=585&page=1'
    }
    return createURLQuery(params, renderingurl);
  }


const processproductJson = (data, chnageData) => {
    for (let index = 0; index < chnageData.length; index++) {
        const element = chnageData[index];
        let placHolderId = data.metadata.template.find(x => x?.purpose == element.purpose)?.id;
        data.document.panels[0].textAreas.find(x => x?.id == placHolderId).content[0].content = element.value
    }
    data.document.panels[0].textAreas[0].content[0].content = Math.random().toString();
    return data;

}
export {restAPICallAsync,createURLQuery,generateRenderingUrl,processproductJson} ;