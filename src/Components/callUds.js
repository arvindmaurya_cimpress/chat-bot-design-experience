export const createDocref = (cimdoc, accessToken) => fetch('https://uds.documents.cimpress.io/v3/documents', {
    method: 'POST',
    headers: {
    'Content-Type': 'application/json',
    Authorization: `Bearer ${ accessToken}`,
    },
    body: JSON.stringify({
        ...cimdoc,
        deleteAfterDays: 1
    }),
}).then((response) => {
    if (response.status === 201) {
    return response.json();
    } 
    throw Error(response.statusText);
    
}).then((data) => Promise.resolve(data._links.previewInstructions.href));
