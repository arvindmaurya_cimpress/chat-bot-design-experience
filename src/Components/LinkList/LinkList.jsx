import React from "react";
import {useContext} from 'react'
import { AppContext } from '../../AppContext';

import "./LinkList.css";

const LinkList = (props) => {
  const { dispatch } = useContext(AppContext);
  return (
    <div className="learning-options-container">
      <button
        className="learning-option-button"
        key={1}
        onClick={() => dispatch({ type: "CLICK", imgUrl: 'option.imgSrc' })}
      >
        <img src={`option.imgSrc`} />
      </button>
    </div>
  );
};

export default LinkList;
