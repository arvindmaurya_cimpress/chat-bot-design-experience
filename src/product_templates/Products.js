export const data = [
  {
    "version": "2.0",
    "fontRepositoryUrl": "https://fonts.documents.cimpress.io/v1/repositories/0a3b1fba-8fa6-43e1-8c3e-a018a5eb8150/published",
    "document": {
      "panels": [
        {
          "id": "23c846d5-b8a1-49e4-b56d-5ec91701affe",
          "decorationTechnology": "offsetOrDigital",
          "name": "Front",
          "width": "91.89999999999999mm",
          "height": "53.8mm",
          "colorMode": "color",
          "images": [
            {
              "id": "03226462-7af5-4a5d-9a42-f891e890491f_vpls_img_2",
              "previewUrl": "https://uploads.documents.cimpress.io/v1/uploads/0e177322-7d04-4acb-b62b-76e3ea923c27~120/original/?tenant=vbu-cl&vpid=9252775",
              "printUrl": "https://uploads.documents.cimpress.io/v1/uploads/18de79fa-17b1-4a79-bf3b-5524ec380428~120/original/?tenant=vbu-cl&vpid=9252775",
              "zIndex": 200,
              "position": {
                "x": "0mm",
                "y": "0mm",
                "width": "91.8986111111111mm",
                "height": "53.7986111111111mm"
              },
              "horizontalAlignment": "left",
              "verticalAlignment": "top",
              "rotationAngle": 0
            },
            {
              "id": "9497d6eb-1726-4b55-ba78-e78d67a6a2b0_vpls_img_1",
              "previewUrl": "https://uploads.documents.cimpress.io/v1/uploads/9333ae9a-0fb4-4a7e-b4c3-32ff101b6c5a~120/original/?tenant=vbu-cl&vpid=9252776",
              "printUrl": "https://uploads.documents.cimpress.io/v1/uploads/e9eb105f-503f-4816-8dd3-31ad70e5e10c~120/original/?tenant=vbu-cl&vpid=9252776",
              "zIndex": 400,
              "position": {
                "x": "38.13822417777778mm",
                "y": "16.292453586111108mm",
                "width": "15.622162755555555mm",
                "height": "15.780864777777774mm"
              },
              "horizontalAlignment": "left",
              "verticalAlignment": "top",
              "rotationAngle": 0
            }
          ],
          "textAreas": [
            {
              "horizontalAlignment": "center",
              "verticalAlignment": "top",
              "blockFlowDirection": "horizontal-tb",
              "zIndex": 600,
              "position": {
                "x": "6.155972222222221mm",
                "y": "34.59280167304145mm",
                "width": "79.58666666666666mm",
                "height": "3.9652mm"
              },
              "rotationAngle": 0,
              "id": "ae93a370-cf5f-4490-89dc-de159bd17a82_vpls_text_3",
              "content": [
                {
                  "content": "COMPANY NAME",
                  "color": "cmyk(0,0,0,50)",
                  "fontFamily": "Josefin Sans",
                  "fontSize": "8.5pt",
                  "lineHeight": "1.3223529411764707em",
                  "fontStyle": "bold"
                }
              ]
            }
          ],
          "shapes": [],
          "itemReferences": [],
          "subpanels": []
        },
        {
          "id": "7b9c0446-e231-442a-aaf3-fa6f5a3c2413",
          "decorationTechnology": "offsetOrDigital",
          "name": "Back",
          "width": "91.89999999999999mm",
          "height": "53.8mm",
          "colorMode": "blank",
          "images": [],
          "textAreas": [],
          "shapes": [],
          "itemReferences": [],
          "subpanels": []
        }
      ]
    },
    "metadata": {
      "ddifVersion": "2.0",
      "template": [
        {
          "locks": {
            "transform": true
          },
          "placeholder": false,
          "imageKey": "9252775",
          "availableFinishes": [],
          "variations": {},
          "id": "03226462-7af5-4a5d-9a42-f891e890491f_vpls_img_2",
          "originalTemplateElementId": "e540664c-bf38-4113-b5c0-f98af82c8c7a_vpls_img_2"
        },
        {
          "locks": {
            "transform": true
          },
          "placeholder": false,
          "imageKey": "9252776",
          "availableFinishes": [
            "metallic",
            "raisedInk"
          ],
          "variations": {
            "rgbCmyk": {
              "previewUrl": "https://uploads.documents.cimpress.io/v1/uploads/9333ae9a-0fb4-4a7e-b4c3-32ff101b6c5a~120/original/?tenant=vbu-cl&vpid=9252776",
              "printUrl": "https://uploads.documents.cimpress.io/v1/uploads/e9eb105f-503f-4816-8dd3-31ad70e5e10c~120/original/?tenant=vbu-cl&vpid=9252776"
            },
            "metallic": {
              "previewUrl": "https://uploads.documents.cimpress.io/v1/uploads/70822af5-b60d-4055-853d-d87d2feeb13d~120/original/?tenant=vbu-cl&vpid=9252776",
              "printUrl": "https://uploads.documents.cimpress.io/v1/uploads/83dc7acb-f08f-4170-a215-427e39438964~120/original/?tenant=vbu-cl&vpid=9252776"
            },
            "raisedInk": {
              "previewUrl": "https://uploads.documents.cimpress.io/v1/uploads/c8760544-51b1-47e8-8a6f-84656023b136~120/original/?tenant=vbu-cl&vpid=9252776",
              "printUrl": "https://uploads.documents.cimpress.io/v1/uploads/f8ebab8e-6d3d-4b52-9cb7-e49ef551a24f~120/original/?tenant=vbu-cl&vpid=9252776"
            }
          },
          "id": "9497d6eb-1726-4b55-ba78-e78d67a6a2b0_vpls_img_1",
          "originalTemplateElementId": "1712ffc2-eeb2-4937-8700-5f6716f123da_vpls_img_1"
        },
        {
          "locks": {},
          "placeholder": "COMPANY NAME",
          "label": "Company Name",
          "purpose": "companyname",
          "availableFinishes": [
            "metallic",
            "raisedInk"
          ],
          "id": "ae93a370-cf5f-4490-89dc-de159bd17a82_vpls_text_3",
          "originalTemplateElementId": "6538e825-8813-4821-9bb9-d64559aa0c52_vpls_text_3"
        }
      ],
      "documentSources": {
        "panels": [
          {
            "id": "23c846d5-b8a1-49e4-b56d-5ec91701affe",
            "source": "TEMPLATE_TOKEN",
            "data": "c3281374..f9c58b2a-e04f-4251-a983-6eb421ba478d"
          }
        ]
      },
      "dclMetadata": [],
      "studioMetadata": {},
      "placeholders": {
        "version": "2",
        "uploads": [],
        "canvases": [
          null,
          null
        ]
      },
      "reviewInstructions": []
    }
  },
  {
    "version": "2.0",
    "fontRepositoryUrl": "https://fonts.documents.cimpress.io/v1/repositories/0a3b1fba-8fa6-43e1-8c3e-a018a5eb8150/published",
    "document": {
      "panels": [
        {
          "id": "23c846d5-b8a1-49e4-b56d-5ec91701affe",
          "decorationTechnology": "offsetOrDigital",
          "name": "Front",
          "width": "94mm",
          "height": "58mm",
          "colorMode": "color",
          "images": [
            {
              "id": "6cba45ab-f3b0-4b71-8edb-92d40735d619_vpls_img_2",
              "previewUrl": "https://uploads.documents.cimpress.io/v1/uploads/cd500235-bff3-4f8f-82cd-9f8220e63025~110/original/?tenant=vbu-cl&vpid=9821031",
              "printUrl": "https://uploads.documents.cimpress.io/v1/uploads/798e35d1-f3ce-43aa-9c43-f251fd7907f4~110/original/?tenant=vbu-cl&vpid=9821031",
              "zIndex": 200,
              "position": {
                "x": "0mm",
                "y": "0mm",
                "width": "93.99763888888889mm",
                "height": "57.99666666666666mm"
              },
              "horizontalAlignment": "left",
              "verticalAlignment": "top",
              "rotationAngle": 0
            },
            {
              "id": "504a1298-af77-412b-987f-71f6b5944338_vpls_img_3",
              "previewUrl": "https://uploads.documents.cimpress.io/v1/uploads/e25af135-8c5b-4a6a-9e4e-09b1d6bf5056~110/original/?tenant=vbu-cl&vpid=9821032",
              "printUrl": "https://uploads.documents.cimpress.io/v1/uploads/6cc95ae2-e842-4654-b465-db3ce4b940de~110/original/?tenant=vbu-cl&vpid=9821032",
              "zIndex": 400,
              "position": {
                "x": "30.928717458333335mm",
                "y": "36.75944444444444mm",
                "width": "32.14020361944444mm",
                "height": "10.936111463888889mm"
              },
              "horizontalAlignment": "left",
              "verticalAlignment": "top",
              "rotationAngle": 0
            },
            {
              "id": "a56bd027-b8d1-44f5-a2d5-a5d14eaa6f99_vpls_img_1",
              "previewUrl": "https://uploads.documents.cimpress.io/v1/uploads/7601a66f-be9a-40f2-bae6-51f36b35c382~110/original/?tenant=vbu-cl&vpid=9821033",
              "printUrl": "https://uploads.documents.cimpress.io/v1/uploads/002c2b23-5ea7-45c0-89e8-4d9715addbcc~110/original/?tenant=vbu-cl&vpid=9821033",
              "zIndex": 600,
              "position": {
                "x": "30.928717458333335mm",
                "y": "11.712221869444443mm",
                "width": "32.14020361944444mm",
                "height": "10.936111463888889mm"
              },
              "horizontalAlignment": "left",
              "verticalAlignment": "top",
              "rotationAngle": 0
            }
          ],
          "textAreas": [
            {
              "horizontalAlignment": "center",
              "verticalAlignment": "top",
              "blockFlowDirection": "horizontal-tb",
              "zIndex": 800,
              "position": {
                "x": "17.365486111111114mm",
                "y": "25.350608270263674mm",
                "width": "59.266666666666666mm",
                "height": "4.3889mm"
              },
              "rotationAngle": 0,
              "id": "34f5daca-b1b5-49a6-9bc7-54f212543721_vpls_text_4",
              "content": [
                {
                  "content": "COMPANY NAME",
                  "color": "cmyk(59,51,50,41)",
                  "fontFamily": "Crete Round",
                  "fontSize": "9.75pt",
                  "lineHeight": "1.1513421474358974em",
                  "fontStyle": "normal"
                }
              ]
            },
            {
              "horizontalAlignment": "center",
              "verticalAlignment": "top",
              "blockFlowDirection": "horizontal-tb",
              "zIndex": 900,
              "position": {
                "x": "17.365486111111114mm",
                "y": "29.70388888888889mm",
                "width": "59.266666666666666mm",
                "height": "6.1784mm"
              },
              "rotationAngle": 0,
              "id": "d6c42f6e-f641-45b8-bf13-ebdbceb5c342_vpls_text_5",
              "content": [
                {
                  "content": "Tagline",
                  "color": "cmyk(59,51,50,41)",
                  "fontFamily": "Sacramento",
                  "fontSize": "12pt",
                  "fontStyle": "normal"
                }
              ]
            }
          ],
          "shapes": [],
          "itemReferences": [],
          "subpanels": []
        },
        {
          "id": "7b9c0446-e231-442a-aaf3-fa6f5a3c2413",
          "decorationTechnology": "offsetOrDigital",
          "name": "Back",
          "width": "94mm",
          "height": "58mm",
          "colorMode": "blank",
          "images": [],
          "textAreas": [],
          "shapes": [],
          "itemReferences": [],
          "subpanels": []
        }
      ]
    },
    "metadata": {
      "ddifVersion": "2.0",
      "template": [
        {
          "locks": {
            "transform": true
          },
          "placeholder": false,
          "imageKey": "9821031",
          "availableFinishes": [],
          "variations": {},
          "id": "6cba45ab-f3b0-4b71-8edb-92d40735d619_vpls_img_2",
          "originalTemplateElementId": "059c033c-f7e4-481a-8409-be37aa32fbc5_vpls_img_2"
        },
        {
          "locks": {
            "transform": true
          },
          "placeholder": false,
          "imageKey": "9821032",
          "availableFinishes": [],
          "variations": {},
          "id": "504a1298-af77-412b-987f-71f6b5944338_vpls_img_3",
          "originalTemplateElementId": "86b86370-0cc6-4c94-b493-2690d00d10d5_vpls_img_3"
        },
        {
          "locks": {
            "transform": true
          },
          "placeholder": false,
          "imageKey": "9821033",
          "availableFinishes": [],
          "variations": {},
          "id": "a56bd027-b8d1-44f5-a2d5-a5d14eaa6f99_vpls_img_1",
          "originalTemplateElementId": "5ba66deb-049b-4579-91ab-3f7008a307be_vpls_img_1"
        },
        {
          "locks": {},
          "placeholder": "COMPANY NAME",
          "label": "Company Name",
          "purpose": "companyname",
          "availableFinishes": [],
          "id": "34f5daca-b1b5-49a6-9bc7-54f212543721_vpls_text_4",
          "originalTemplateElementId": "c08ce092-a923-42b6-bbe7-6278a4e56ba9_vpls_text_4"
        },
        {
          "locks": {},
          "placeholder": "business type",
          "label": "Business Type",
          "purpose": "businesstype",
          "availableFinishes": [],
          "id": "d6c42f6e-f641-45b8-bf13-ebdbceb5c342_vpls_text_5",
          "originalTemplateElementId": "572c3cfb-73da-4a49-a395-6c892d0a5399_vpls_text_5"
        }
      ],
      "documentSources": {
        "panels": [
          {
            "id": "23c846d5-b8a1-49e4-b56d-5ec91701affe",
            "source": "TEMPLATE_TOKEN",
            "data": "c6054643..c268fe7b-3c71-4a06-9f3e-3e47227bef55"
          }
        ]
      },
      "dclMetadata": [],
      "studioMetadata": {},
      "placeholders": {
        "version": "2",
        "uploads": [],
        "canvases": [
          null,
          null
        ]
      },
      "reviewInstructions": []
    }
  },
  {
    "metadata": {
        "idtifVersion": "2.6",
        "template": [
            {
                "locks": {},
                "placeholder": "PHONE / OTHER",
                "label": "Phone / Other",
                "purpose": "phone",
                "id": "63b99a80-c440-4798-86ba-631a19bbf934",
                "originalTemplateElementId": "63b99a80-c440-4798-86ba-631a19bbf934"
            },
            {
                "locks": {},
                "placeholder": "BUSINESS TYPE",
                "label": "Business Type",
                "purpose": "businesstype",
                "id": "0b226f60-94fb-4711-8489-d81c0b209c9a",
                "originalTemplateElementId": "0b226f60-94fb-4711-8489-d81c0b209c9a"
            },
            {
                "locks": {},
                "placeholder": "COMPANY NAME",
                "label": "Company Name",
                "purpose": "companyname",
                "id": "00a5a40d-aea8-4c2d-a1e9-8de406c5ee9e",
                "originalTemplateElementId": "00a5a40d-aea8-4c2d-a1e9-8de406c5ee9e"
            },
            {
                "locks": {
                    "transform": true
                },
                "placeholder": false,
                "id": "9b7505f6-1475-4a86-ba4a-d4a8604a6725",
                "originalTemplateElementId": "9b7505f6-1475-4a86-ba4a-d4a8604a6725"
            },
            {
                "locks": {
                    "transform": true
                },
                "id": "342",
                "originalTemplateElementId": "342"
            }
        ]
    },
    "version": "2.0",
    "document": {
        "panels": [
            {
                "width": "66.67500000000089mm",
                "height": "66.67500000000089mm",
                "decorationTechnology": "offsetOrDigital",
                "id": "5488c212-dd7d-447c-9399-dbd619296e4d",
                "name": "s21327a9a-6e38-46cd-8333-dc3f4652de5b:v1..14a66078-9e03-4465-a119-1cb3c71bbb0e",
                "textAreas": [
                    {
                        "horizontalAlignment": "center",
                        "verticalAlignment": "top",
                        "zIndex": 300,
                        "position": {
                            "x": "10.407498785782385mm",
                            "y": "51.680546625028654mm",
                            "width": "45.859994649654695mm",
                            "height": "3.5277773662037513mm"
                        },
                        "rotationAngle": 0,
                        "textFields": [
                            {
                                "id": "63b99a80-c440-4798-86ba-631a19bbf934",
                                "fontFamily": "Readex Pro",
                                "fontStyle": "normal",
                                "fontSize": "8pt",
                                "content": "PHONE / OTHER",
                                "color": "cmyk(0, 0, 0, 0)",
                                "overprints": []
                            }
                        ]
                    },
                    {
                        "horizontalAlignment": "center",
                        "verticalAlignment": "top",
                        "zIndex": 200,
                        "position": {
                            "x": "3.6580690176172967mm",
                            "y": "34.65280534266127mm",
                            "width": "59.35885418598484mm",
                            "height": "4.850693878530013mm"
                        },
                        "rotationAngle": 0,
                        "textFields": [
                            {
                                "id": "0b226f60-94fb-4711-8489-d81c0b209c9a",
                                "fontFamily": "Readex Pro",
                                "fontStyle": "normal",
                                "fontSize": "11pt",
                                "content": "BUSINESS TYPE",
                                "color": "cmyk(41, 0, 54, 19)",
                                "overprints": []
                            }
                        ]
                    }
                ],
                "images": [
                    {
                        "id": "9b7505f6-1475-4a86-ba4a-d4a8604a6725",
                        "previewUrl": "https://uploads.documents.cimpress.io/v1/uploads/ba49c21b-ea0b-4570-aed7-d69f1595e93c~110/preview?tenant=vbu-cl",
                        "printUrl": "https://uploads.documents.cimpress.io/v1/uploads/ba49c21b-ea0b-4570-aed7-d69f1595e93c~110/print?tenant=vbu-cl",
                        "zIndex": 500,
                        "position": {
                            "x": "25.977046908048163mm",
                            "y": "11.46666822998713mm",
                            "width": "14.720898405123213mm",
                            "height": "14.081138174886352mm"
                        },
                        "rotationAngle": 0
                    }
                ],
                "itemReferences": [
                    {
                        "id": "00a5a40d-aea8-4c2d-a1e9-8de406c5ee9e",
                        "type": "Word Art",
                        "url": "https://udsinterop.document.vpsvc.com/api/itemref/wordart",
                        "zIndex": 400,
                        "position": {
                            "x": "3.658069017617332mm",
                            "y": "27.983639454045157mm",
                            "width": "59.358854185984804mm",
                            "height": "5.291666049305627mm"
                        },
                        "rotationAngle": 0,
                        "data": {
                            "fontFamily": "Barlow Semi Condensed SemiBold",
                            "fontStyle": "normal",
                            "content": "COMPANY NAME",
                            "color": "cmyk(0, 0, 0, 0)",
                            "focus": "center",
                            "stroke": {
                                "thickness": 0
                            },
                            "shadow": {
                                "xoffset": 0,
                                "yoffset": 0
                            },
                            "curve": {
                                "radius": 0,
                                "height": 0,
                                "angle": 0
                            }
                        }
                    }
                ],
                "shapes": [
                    {
                        "id": "342",
                        "type": "rectangle",
                        "zIndex": 100,
                        "color": "cmyk(92, 77, 49, 21)",
                        "position": {
                            "x": "0mm",
                            "y": "0mm",
                            "width": "66.67499222121951mm",
                            "height": "66.67499222121951mm"
                        },
                        "stroke": {
                            "color": "cmyk(0, 0, 0, 0)",
                            "thickness": "0mm"
                        },
                        "rotationAngle": 0
                    }
                ]
            }
        ]
    },
    "fontRepositoryUrl": "https://fonts.documents.cimpress.io/v1/repositories/0a3b1fba-8fa6-43e1-8c3e-a018a5eb8150/published"
}

]
