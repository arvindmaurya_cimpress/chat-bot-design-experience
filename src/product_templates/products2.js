export const data = [
    {
        "metadata": {
            "idtifVersion": "2.6",
            "template": [
                {
                    "locks": {
                        "edit": true
                    },
                    "placeholder": false,
                    "imageKey": "980326",
                    "availableFinishes": [],
                    "variations": {},
                    "id": "2167ae43-1136-4b7e-8c44-b6719ceb6032_vpls_img_1",
                    "originalTemplateElementId": "2167ae43-1136-4b7e-8c44-b6719ceb6032_vpls_img_1"
                },
                {
                    "placeholder": "Company Name",
                    "label": "Company Name",
                    "purpose": "companyname",
                    "availableFinishes": [],
                    "id": "4be0201c-fe35-4921-bfbe-ddc32745f7f3_vpls_text_2",
                    "originalTemplateElementId": "4be0201c-fe35-4921-bfbe-ddc32745f7f3_vpls_text_2"
                },
                {
                    "placeholder": "Job Title",
                    "label": "Job Title",
                    "purpose": "jobtitle",
                    "availableFinishes": [],
                    "id": "30c2d4ed-f5b6-4db7-b5e9-54b9d1f61712_vpls_text_3",
                    "originalTemplateElementId": "30c2d4ed-f5b6-4db7-b5e9-54b9d1f61712_vpls_text_3"
                },
                {
                    "placeholder": "Full Name",
                    "label": "Full Name",
                    "purpose": "fullname",
                    "availableFinishes": [],
                    "id": "a7439b23-681b-4aa5-8cd2-a2e9241380b8_vpls_text_4",
                    "originalTemplateElementId": "a7439b23-681b-4aa5-8cd2-a2e9241380b8_vpls_text_4"
                },
                {
                    "placeholder": "Address Line 1",
                    "label": "Address Line 1",
                    "purpose": "address1",
                    "availableFinishes": [],
                    "id": "7ea75575-f51d-41be-926c-6736c38bfa60_vpls_text_5",
                    "originalTemplateElementId": "7ea75575-f51d-41be-926c-6736c38bfa60_vpls_text_5"
                },
                {
                    "placeholder": "Address Line 2",
                    "label": "Address Line 2",
                    "purpose": "address2",
                    "availableFinishes": [],
                    "id": "38aaee82-2dab-48e8-b378-a7976b92373b_vpls_text_6",
                    "originalTemplateElementId": "38aaee82-2dab-48e8-b378-a7976b92373b_vpls_text_6"
                },
                {
                    "placeholder": "Email / Other",
                    "label": "Email / Other",
                    "purpose": "email",
                    "availableFinishes": [],
                    "id": "9ee583bb-82c5-487b-9146-ed1a4001cf37_vpls_text_7",
                    "originalTemplateElementId": "9ee583bb-82c5-487b-9146-ed1a4001cf37_vpls_text_7"
                },
                {
                    "placeholder": "Web / Other",
                    "label": "Web / Other",
                    "purpose": "web",
                    "availableFinishes": [],
                    "id": "4bb7b89b-0e4f-450d-8685-340d51507064_vpls_text_8",
                    "originalTemplateElementId": "4bb7b89b-0e4f-450d-8685-340d51507064_vpls_text_8"
                },
                {
                    "placeholder": "Phone / Other",
                    "label": "Phone / Other",
                    "purpose": "phone",
                    "availableFinishes": [],
                    "id": "ad7a7586-d7a4-40d5-9056-755ec2dd35cc_vpls_text_9",
                    "originalTemplateElementId": "ad7a7586-d7a4-40d5-9056-755ec2dd35cc_vpls_text_9"
                }
            ]
        },
        "version": "2.0",
        "document": {
            "panels": [
                {
                    "width": "91.89861111111236mm",
                    "height": "53.79861111111184mm",
                    "decorationTechnology": "offsetOrDigital",
                    "id": "6c64ba57-81dc-44b2-8522-a2f516c0c587",
                    "name": "c1751263..f9c58b2a-e04f-4251-a983-6eb421ba478d",
                    "textAreas": [
                        {
                            "horizontalAlignment": "left",
                            "verticalAlignment": "top",
                            "blockFlowDirection": "horizontal-tb",
                            "zIndex": 200,
                            "position": {
                                "x": "41.01041666666666mm",
                                "y": "9.955281867622343mm",
                                "width": "45.86111111111111mm",
                                "height": "5.044308810763889mm"
                            },
                            "rotationAngle": 0,
                            "textFields": [
                                {
                                    "id": "4be0201c-fe35-4921-bfbe-ddc32745f7f3_vpls_text_2",
                                    "fontFamily": "Teko",
                                    "fontStyle": "normal",
                                    "fontSize": "12.899999999999999pt",
                                    "content": "Company Name",
                                    "color": "cmyk(3,5,9,0)",
                                    "lineHeight": "1.0809302325581398em"
                                }
                            ]
                        },
                        {
                            "horizontalAlignment": "left",
                            "verticalAlignment": "top",
                            "blockFlowDirection": "horizontal-tb",
                            "zIndex": 300,
                            "position": {
                                "x": "41.01041666666666mm",
                                "y": "29.942513204956054mm",
                                "width": "45.86111111111111mm",
                                "height": "4.232988823784722mm"
                            },
                            "rotationAngle": 0,
                            "textFields": [
                                {
                                    "id": "30c2d4ed-f5b6-4db7-b5e9-54b9d1f61712_vpls_text_3",
                                    "fontFamily": "Teko Light",
                                    "fontStyle": "normal",
                                    "fontSize": "10.75pt",
                                    "content": "Job Title",
                                    "color": "cmyk(3,5,9,0)",
                                    "lineHeight": "1.078139534883721em"
                                }
                            ]
                        },
                        {
                            "horizontalAlignment": "left",
                            "verticalAlignment": "top",
                            "blockFlowDirection": "horizontal-tb",
                            "zIndex": 400,
                            "position": {
                                "x": "41.01041666666666mm",
                                "y": "25.33140472445041mm",
                                "width": "45.86111111111111mm",
                                "height": "4.638648817274306mm"
                            },
                            "rotationAngle": 0,
                            "textFields": [
                                {
                                    "id": "a7439b23-681b-4aa5-8cd2-a2e9241380b8_vpls_text_4",
                                    "fontFamily": "Teko",
                                    "fontStyle": "normal",
                                    "fontSize": "11.825pt",
                                    "content": "Full Name",
                                    "color": "cmyk(3,5,9,0)",
                                    "lineHeight": "1.0809302325581398em"
                                }
                            ]
                        },
                        {
                            "horizontalAlignment": "left",
                            "verticalAlignment": "top",
                            "blockFlowDirection": "horizontal-tb",
                            "zIndex": 500,
                            "position": {
                                "x": "41.01041666666666mm",
                                "y": "14.94945690765381mm",
                                "width": "45.86111111111111mm",
                                "height": "4.232988823784722mm"
                            },
                            "rotationAngle": 0,
                            "textFields": [
                                {
                                    "id": "7ea75575-f51d-41be-926c-6736c38bfa60_vpls_text_5",
                                    "fontFamily": "Teko Light",
                                    "fontStyle": "normal",
                                    "fontSize": "10.75pt",
                                    "content": "Address Line 1",
                                    "color": "cmyk(3,5,9,0)",
                                    "lineHeight": "1.078139534883721em"
                                }
                            ]
                        },
                        {
                            "horizontalAlignment": "left",
                            "verticalAlignment": "top",
                            "blockFlowDirection": "horizontal-tb",
                            "zIndex": 600,
                            "position": {
                                "x": "41.01041666666666mm",
                                "y": "19.18279144897461mm",
                                "width": "45.86111111111111mm",
                                "height": "4.232988823784722mm"
                            },
                            "rotationAngle": 0,
                            "textFields": [
                                {
                                    "id": "38aaee82-2dab-48e8-b378-a7976b92373b_vpls_text_6",
                                    "fontFamily": "Teko Light",
                                    "fontStyle": "normal",
                                    "fontSize": "10.75pt",
                                    "content": "Address Line 2",
                                    "color": "cmyk(3,5,9,0)",
                                    "lineHeight": "1.078139534883721em"
                                }
                            ]
                        },
                        {
                            "horizontalAlignment": "left",
                            "verticalAlignment": "top",
                            "blockFlowDirection": "horizontal-tb",
                            "zIndex": 700,
                            "position": {
                                "x": "41.01041666666666mm",
                                "y": "34.17584488525391mm",
                                "width": "45.86111111111111mm",
                                "height": "4.232988823784722mm"
                            },
                            "rotationAngle": 0,
                            "textFields": [
                                {
                                    "id": "9ee583bb-82c5-487b-9146-ed1a4001cf37_vpls_text_7",
                                    "fontFamily": "Teko Light",
                                    "fontStyle": "normal",
                                    "fontSize": "10.75pt",
                                    "content": "Email / Other",
                                    "color": "cmyk(3,5,9,0)",
                                    "lineHeight": "1.078139534883721em"
                                }
                            ]
                        },
                        {
                            "horizontalAlignment": "left",
                            "verticalAlignment": "top",
                            "blockFlowDirection": "horizontal-tb",
                            "zIndex": 800,
                            "position": {
                                "x": "41.01041666666666mm",
                                "y": "40.34945557250977mm",
                                "width": "45.86111111111111mm",
                                "height": "4.232988823784722mm"
                            },
                            "rotationAngle": 0,
                            "textFields": [
                                {
                                    "id": "4bb7b89b-0e4f-450d-8685-340d51507064_vpls_text_8",
                                    "fontFamily": "Teko Light",
                                    "fontStyle": "normal",
                                    "fontSize": "10.75pt",
                                    "content": "Web / Other",
                                    "color": "cmyk(3,5,9,0)",
                                    "lineHeight": "1.078139534883721em"
                                }
                            ]
                        },
                        {
                            "horizontalAlignment": "left",
                            "verticalAlignment": "top",
                            "blockFlowDirection": "horizontal-tb",
                            "zIndex": 900,
                            "position": {
                                "x": "41.01041666666666mm",
                                "y": "44.94086021525065mm",
                                "width": "45.86111111111111mm",
                                "height": "4.232988823784722mm"
                            },
                            "rotationAngle": 0,
                            "textFields": [
                                {
                                    "id": "ad7a7586-d7a4-40d5-9056-755ec2dd35cc_vpls_text_9",
                                    "fontFamily": "Teko",
                                    "fontStyle": "normal",
                                    "fontSize": "10.75pt",
                                    "content": "Phone / Other",
                                    "color": "cmyk(3,5,9,0)",
                                    "lineHeight": "1.0809302325581398em"
                                }
                            ]
                        }
                    ],
                    "images": [
                        {
                            "id": "2167ae43-1136-4b7e-8c44-b6719ceb6032_vpls_img_1",
                            "previewUrl": "https://uploads.documents.cimpress.io/v1/uploads/9e62de05-ba64-407f-a007-40a2144c86ab~110/original/?tenant=vbu-cl&vpid=980326",
                            "printUrl": "https://uploads.documents.cimpress.io/v1/uploads/41dd53d2-a0f0-4b80-91c0-be2cbbe7760d~110/original/?tenant=vbu-cl&vpid=980326",
                            "zIndex": 100,
                            "position": {
                                "x": "0mm",
                                "y": "0mm",
                                "width": "91.8986111111111mm",
                                "height": "53.7986111111111mm"
                            },
                            "horizontalAlignment": "left",
                            "verticalAlignment": "top",
                            "rotationAngle": 0
                        }
                    ],
                    "itemReferences": [],
                    "shapes": []
                }
            ]
        },
        "fontRepositoryUrl": "https://fonts.documents.cimpress.io/v1/repositories/0a3b1fba-8fa6-43e1-8c3e-a018a5eb8150/published"
    },
    {
        "metadata": {
            "idtifVersion": "2.6",
            "template": [
                {
                    "locks": {},
                    "placeholder": "BUSINESS TYPE",
                    "label": "Business Type",
                    "purpose": "businesstype",
                    "availableFinishes": [
                        "raisedInk"
                    ],
                    "id": "fde1f556-a538-44d6-8cee-b9786cb3221f",
                    "originalTemplateElementId": "fde1f556-a538-44d6-8cee-b9786cb3221f"
                },
                {
                    "locks": {},
                    "placeholder": "COMPANY NAME",
                    "label": "Company Name",
                    "purpose": "companyname",
                    "availableFinishes": {
                        "fill": [
                            "raisedInk"
                        ],
                        "stroke": [
                            "raisedInk"
                        ]
                    },
                    "id": "b96e4139-59cc-4aea-9821-22052778a357",
                    "originalTemplateElementId": "b96e4139-59cc-4aea-9821-22052778a357"
                },
                {
                    "locks": {
                        "transform": true
                    },
                    "placeholder": false,
                    "id": "a9ea83de-9303-41be-b36b-4a2b5bf3f0fb",
                    "originalTemplateElementId": "a9ea83de-9303-41be-b36b-4a2b5bf3f0fb"
                },
                {
                    "locks": {
                        "transform": true
                    },
                    "placeholder": false,
                    "id": "04a2fff2-f88e-451c-88ab-5bcef37d950f",
                    "originalTemplateElementId": "04a2fff2-f88e-451c-88ab-5bcef37d950f"
                }
            ]
        },
        "version": "2.0",
        "document": {
            "panels": [
                {
                    "width": "91.72000000000125mm",
                    "height": "53.62000000000073mm",
                    "decorationTechnology": "offsetOrDigital",
                    "id": "5488c212-dd7d-447c-9399-dbd619296e4d",
                    "name": "scdfcb6ff-ace9-4ff5-8513-d78f6910d2d8:v1..8bc979ba-7a4b-4c3b-8274-97e7e733d94a",
                    "textAreas": [
                        {
                            "horizontalAlignment": "center",
                            "verticalAlignment": "top",
                            "zIndex": 300,
                            "position": {
                                "x": "10.582220987607942mm",
                                "y": "30.425968672513214mm",
                                "width": "70.5555473240754mm",
                                "height": "5.291666049305662mm"
                            },
                            "rotationAngle": 0,
                            "textFields": [
                                {
                                    "id": "fde1f556-a538-44d6-8cee-b9786cb3221f",
                                    "fontFamily": "Poppins Light",
                                    "fontStyle": "normal",
                                    "fontSize": "10pt",
                                    "content": "BUSINESS TYPE",
                                    "color": "cmyk(60, 40, 40, 80)",
                                    "overprints": [],
                                    "letterspacing": "0.3em"
                                }
                            ]
                        }
                    ],
                    "images": [
                        {
                            "id": "a9ea83de-9303-41be-b36b-4a2b5bf3f0fb",
                            "previewUrl": "https://uploads.documents.cimpress.io/v1/uploads/89f19cb6-8e68-4f87-b02d-d3737074a0fd~110?tenant=vbu-cl",
                            "printUrl": "https://uploads.documents.cimpress.io/v1/uploads/300bc7ea-8909-4eac-8119-5d0f5ac84e03~110/print?tenant=vbu-cl",
                            "zIndex": 200,
                            "position": {
                                "x": "28.221107818626876mm",
                                "y": "9.171110041135613mm",
                                "width": "35.27777366203737mm",
                                "height": "35.277773662037546mm"
                            },
                            "rotationAngle": 0
                        },
                        {
                            "id": "04a2fff2-f88e-451c-88ab-5bcef37d950f",
                            "previewUrl": "https://uploads.documents.cimpress.io/v1/uploads/19f3cfbb-11e5-4626-8e7c-7e11567d942e~110/preview?tenant=vbu-cl",
                            "printUrl": "https://uploads.documents.cimpress.io/v1/uploads/19f3cfbb-11e5-4626-8e7c-7e11567d942e~110/print?tenant=vbu-cl",
                            "zIndex": 100,
                            "position": {
                                "x": "0mm",
                                "y": "0mm",
                                "width": "91.71998929929134mm",
                                "height": "53.61999374430881mm"
                            },
                            "rotationAngle": 0
                        }
                    ],
                    "itemReferences": [
                        {
                            "id": "b96e4139-59cc-4aea-9821-22052778a357",
                            "type": "Word Art",
                            "url": "https://udsinterop.document.vpsvc.com/api/itemref/wordart",
                            "zIndex": 400,
                            "position": {
                                "x": "10.58222098760801mm",
                                "y": "21.783469680811113mm",
                                "width": "70.55554732407533mm",
                                "height": "7.0555547324075025mm"
                            },
                            "rotationAngle": 0,
                            "data": {
                                "fontFamily": "C4 v.4",
                                "fontStyle": "normal",
                                "content": "COMPANY NAME",
                                "color": "cmyk(60, 40, 40, 80)",
                                "focus": "center",
                                "stroke": {
                                    "color": "cmyk(60, 40, 40, 80)",
                                    "thickness": 0.015
                                },
                                "shadow": {
                                    "xoffset": 0,
                                    "yoffset": 0
                                },
                                "curve": {
                                    "radius": 0,
                                    "height": 0,
                                    "angle": 0
                                }
                            }
                        }
                    ],
                    "shapes": []
                }
            ]
        },
        "fontRepositoryUrl": "https://fonts.documents.cimpress.io/v1/repositories/0a3b1fba-8fa6-43e1-8c3e-a018a5eb8150/published"
    },
    
]
