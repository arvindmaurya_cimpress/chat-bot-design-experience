
import './App.css';
import { GlobalStyles } from '@cimpress/react-components';
import DesignExperienceChatBot from './Components/ChatBotComponent';
import DesignExperiencePreview from './Components/DesignExperiencePreview'
import { useReducer } from 'react';
import { AppContext, initialState, reducer } from './AppContext';

function App() {

  const [state,dispatch] = useReducer(reducer, initialState);

  return (
    <AppContext.Provider value={{ ...state, dispatch }}>
      <div>
        <GlobalStyles />
        <div className='header'> Chat bot Design Experience</div>

        <div className='container'>
          <div className='left-section'>
            <DesignExperienceChatBot/>
          </div>
          <div className='right-section'>
            <DesignExperiencePreview/>
          </div>
        </div>
      </div>
    </AppContext.Provider>
   
  );
}

export default App;
