import { createContext } from 'react';

// Create a context with an initial state
export const AppContext = createContext();

// Define the initial state and a reducer function
export const initialState = {
    imgUrl: '',
};



export const reducer = (state, action) => {
    debugger;
    switch (action.type) {
      case 'CLICK':
        return {...state, imgUrl: action.imgUrl}
      default:
        return state;
    }
  };